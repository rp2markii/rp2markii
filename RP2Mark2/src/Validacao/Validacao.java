/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validacao;

import Listas.CrudMidia;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Matheus Marques
 * Matrícula: 171570595
 */
public class Validacao {

    /**
     * Valida Strings
     * @return String
     */
    public static String validarString() {
        Scanner e = new Scanner(System.in);
        String descricao;
        do {
            descricao = e.nextLine();
            Pattern pattern = Pattern.compile("[@#$%¨&*()_+!,*]");
            Matcher matcher = pattern.matcher(descricao);
            if (descricao == null || " ".equals(descricao) || "".equals(descricao)) {
                System.err.println("Informação inválida!");
            }
            if (matcher.find()) {
                System.err.println("Informação inválida!");
                descricao = null;
            }
        } while (descricao == null || " ".equals(descricao) || "".equals(descricao));
        return descricao;
    }

    /**
     * Valida double
     * @return double
     */
    public static double validarDouble() {
        Scanner e = new Scanner(System.in);
        boolean retorno;
        double valorDouble = 0;
        do {
            try {
                valorDouble = Double.parseDouble(e.next().replaceAll(":", "."));
                return valorDouble;
            } catch (NumberFormatException s) {
                System.err.println("Numero inválido");
                retorno = true;
            }
        } while (retorno);
        return valorDouble;
    }

    /**
     * Valida int
     * @return int
     */
    public static int validarInteiro() {
        Scanner e = new Scanner(System.in);
        int valorInteiro = 0;
        boolean retorno;
        do {
            try {
                valorInteiro = Integer.parseInt(e.next());
                return valorInteiro;
            } catch (NumberFormatException exception) {
                System.err.println("Digite um numero válido!");
                retorno = true;
            }
        } while (retorno);
        return valorInteiro;
    }

    /**
     * Valida ano
     * @return int 
     */
    public static int validarAno() {
        Scanner e = new Scanner(System.in);
        int valorInteiro = 0;
        boolean retorno;
        do {
            try {
                do {
                    valorInteiro = Integer.parseInt(e.next());
                    if(valorInteiro>3000)
                        System.err.println("Digite um ano válido");
                } while (valorInteiro > 3000);
                return valorInteiro;
            } catch (NumberFormatException exception) {
                System.err.println("Digite um ano válido!");
                retorno = true;
            }
        } while (retorno);
        return valorInteiro;
    }

    /**
     * Valida data
     * @return String
     */
    public static String validarValidade() {
        Scanner e = new Scanner(System.in);
        String validade = null;
        Calendar calendar = Calendar.getInstance();
        Date data;
        boolean retorno;
        do {
            try {
                validade = e.nextLine();
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                data = df.parse(validade);
                calendar.setTime(data);
                return df.format(calendar.getTime());
            } catch (ParseException ex) {
                System.err.println("Digite uma data dentro do formato dd/mm/AAAA");
                retorno = true;
            }
        } while (validade == null || " ".equals(validade) || "".equals(validade) || retorno);
        return null;
    }

    /**
     * Inicializa sistema.
     * @param crudMidia
     */
    public static void inicializar(CrudMidia[] crudMidia) {
        for (CrudMidia crudMidiaV : crudMidia) {
            try {
                crudMidiaV.carregar();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("Arquivos inicializados com sucesso!");
    }
}
