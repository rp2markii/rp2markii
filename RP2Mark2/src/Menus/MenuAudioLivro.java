package Menus;

import Validacao.Validacao;
import Listas.CrudMidia;
import Midias.Audiolivro;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuAudioLivro extends MenuMidias {

    public MenuAudioLivro(CrudMidia crud) {
        super(crud);
    }

    /**
     * Cadastra as midias que o usuario deseja guardar
     */
    @Override
    public void cadastrar() {
        String genero;
        String idioma;
        List<String> autores = new ArrayList<>();
        String local;
        String editora;
        String descricao;
        String caminhoDoArquivo;
        String titulo;
        int escolha;
        double duracao;
        int ano;
        System.out.println("Menu de Cadastro");
        System.out.println("Titulo do Audiolivro: ");
        titulo = Validacao.validarString();
        System.out.println("Descrição do Audiolivro: ");
        descricao = Validacao.validarString();
        System.out.println("Caminho do Audiolivro: ");
        caminhoDoArquivo = Validacao.validarString();
        System.out.println("Gênero do Audiolivro:");
        genero = Validacao.validarString();
        System.out.println("Idioma do Audiolivro:");
        idioma = Validacao.validarString();
        System.out.println("Autor do Audiolivro:");
        autores.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro autor?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro autor");
                    autores.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);

        System.out.println("Local do Audiolivro:");
        local = Validacao.validarString();
        System.out.println("Editora do Audiolivro:");
        editora = Validacao.validarString();
        System.out.println("Duração do Audiolivro:");
        duracao = Validacao.validarDouble();
        System.out.println("Ano do Audiolivro:");
        ano = Validacao.validarAno();
        Audiolivro audiolivro = new Audiolivro(caminhoDoArquivo, titulo, descricao, genero, idioma, autores, local, editora, duracao, ano);
        if (super.crudMidia.cadastrarMidia(audiolivro) == true) {
            System.out.println("Mídia cadastrada com sucesso!");
        } else {
            System.out.println("Erro no cadastro de mídia!");

        }
    }

    /**
     * permite o usuario editar suas midias
     */
    @Override
    public void editar() {
        int escolha;
        Scanner entrada = new Scanner(System.in);
        List<String> autores = new ArrayList<>();
        System.out.println("Menu de edição");
        System.out.println("Qual midia você deseja editar:");
        String tituloBusca = Validacao.validarString();
        
        
        Audiolivro velho = (Audiolivro) super.crudMidia.pesquisarMidia(tituloBusca);
        if (velho != null) {
            System.out.println("Digite o novo caminho do arquivo:");
            String caminhoDoArquivo = entrada.nextLine();
            if (caminhoDoArquivo.equalsIgnoreCase("")) {
                caminhoDoArquivo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite o novo titulo:");
            String titulo = entrada.nextLine();
            if (titulo.equalsIgnoreCase("")) {
                titulo = velho.getTitulo();
            }
            System.out.println("Digite a nova descrição:");
            String descricao = entrada.nextLine();
            if (descricao.equalsIgnoreCase("")) {
                descricao = velho.getDescricao();
            }
            System.out.println("Digite o novo gênero:");
            String genero = entrada.nextLine();
            if (genero.equalsIgnoreCase("")) {
                genero = velho.getGenero();
            }
            System.out.println("Digite o novo idioma:");
            String idioma = entrada.nextLine();
            if (idioma.equalsIgnoreCase("")) {
                idioma = velho.getIdioma();
            }
            System.out.println("Digite novos autores");
            autores.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro autor?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = entrada.nextInt();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro autor");
                        autores.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("Digite o novo local:");
            String local = entrada.nextLine();
            if (local.equalsIgnoreCase("")) {
                local = velho.getLocal();
            }
            System.out.println("Digite a nova editora");
            String editora = entrada.nextLine();
            if (editora.equalsIgnoreCase("")) {
                editora = velho.getEditora();
            }
            System.out.println("Digite o novo número de páginas:");
            double duracao;
            try {
                duracao = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                duracao = velho.getDuracao();
            }
            System.out.println("Digite o novo ano:");
            int ano;
            try {
                ano = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                ano = velho.getAno();
            }

            super.crudMidia.cadastrarMidia(new Audiolivro(caminhoDoArquivo, titulo, descricao, genero, idioma, autores, local, editora, duracao, ano));
        } else {
            System.out.println("Mídia não encontrada!");
        }

    }

}
