package Menus;

import Listas.CrudMidia;
import Midias.Filme;
import Validacao.Validacao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuFilme extends MenuMidias {

    public MenuFilme(CrudMidia crud) {
        super(crud);
    }

    @Override
    public void cadastrar() {
        String genero;
        String idioma;
        String diretor;
        List<String> atoresPrincipais = new ArrayList<>();
        String descricao;
        String caminhoDoArquivo;
        String titulo;
        int escolha;
        double duracao;
        int ano;
        System.out.println("Menu de Cadastro");
        System.out.println("Titulo do Filme");
        titulo = Validacao.validarString();
        System.out.println("Descrição do filme: ");
        descricao = Validacao.validarString();
        System.out.println("Caminho do Filme: ");
        caminhoDoArquivo = Validacao.validarString();
        System.out.println("Genero do filme:");
        genero = Validacao.validarString();
        System.out.println("Idioma do filme:");
        idioma = Validacao.validarString();
        System.out.println("Diretor do filme:");
        diretor = Validacao.validarString();
        System.out.println("Atores principais do filme:");
        atoresPrincipais.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro ator?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro ator");
                    atoresPrincipais.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);
        System.out.println("Duração do filme:");
        duracao = Validacao.validarDouble();
        System.out.println("Ano do filme:");
        ano = Validacao.validarAno();
        Filme filmes = new Filme(caminhoDoArquivo, titulo, descricao, genero, idioma, diretor, atoresPrincipais, duracao, ano);
        if (super.crudMidia.cadastrarMidia(filmes) == true) {
            System.out.println("Cadastro efetuado com sucesso.");
        } else {
            System.out.println("Erro ao cadastrar o arquivo.");
        }

    }

    @Override
    public void editar() {
        int escolha;
        Scanner entrada = new Scanner(System.in);
        List<String> atoresPrincipais = new ArrayList();
        System.out.println("Menu de edição");
        System.out.println("Qual mídia você deseja editar:");
        String titulo = Validacao.validarString();
        
       
        Filme velho = (Filme) super.crudMidia.pesquisarMidia(titulo);
        if (velho != null) {
            System.out.println("Digite o novo caminho:");
            String caminhoDoArquivo = entrada.nextLine();
            if (caminhoDoArquivo.equalsIgnoreCase("")) {
                caminhoDoArquivo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite o novo titulo:");
            titulo = entrada.nextLine();
            if (titulo.equalsIgnoreCase("")) {
                titulo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite a nova descrição:");
            String descricao = entrada.nextLine();
            if (descricao.equalsIgnoreCase("")) {
                descricao = velho.getDescricao();
            }
            System.out.println("Digite o novo gênero:");
            String genero = entrada.nextLine();
            if (genero.equalsIgnoreCase("")) {
                genero = velho.getGenero();
            }
            System.out.println("Digite o novo idioma:");
            String idioma = entrada.nextLine();
            if (idioma.equalsIgnoreCase("")) {
                idioma = velho.getIdioma();
            }
            System.out.println("Digite o novo diretor:");
            String diretor = entrada.nextLine();
            if (diretor.equalsIgnoreCase("")) {
                diretor = velho.getDiretor();
            }
            System.out.println("Digite novos autores");
            atoresPrincipais.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro autor?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = Validacao.validarInteiro();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro autor");
                        atoresPrincipais.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("duração");
            double duracao;
            try {
                duracao = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                duracao = velho.getDuracao();
            }
            System.out.println("Digite o novo ano de lançamento:");
            int ano;
            try {
                ano = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                ano = velho.getAno();
            }
            super.crudMidia.cadastrarMidia(new Filme(caminhoDoArquivo, titulo, descricao, genero, idioma, diretor, atoresPrincipais, duracao, ano));

        } else {
            System.out.println("Mídia não encontrada!");
        }
    }

}
