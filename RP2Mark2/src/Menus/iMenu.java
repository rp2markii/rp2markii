/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menus;

/**
 *
 * @author Matheus Marques
 */
public interface iMenu {
    
    /**
     * Menu de que leva a outros menus, que darão as principais escolhas de uso do programa para os usuários.
     */
    public abstract void menu();
    /**
     * Menu de cadastramento de um arquivo (MIDIA)
     */
    public abstract void cadastrar();

    /**
     * Menu de edição de um arquivo (MIDIA)
     */
    public abstract void editar();

    /**
     * Menu de exclusão de um arquivo (MIDIA)
     */
    public abstract void excluir();

    /**
     * Menu de consulta de um arquivo (MIDIA)
     */
    public abstract void consultar();

    /**
     * Menu de salvamento de um arquivo (MIDIA)
     */
    public abstract void salvar();

    /**
     * Exibe as midias que contem o parametro passado
     * @param titulo_descricao
     * @return True se o titulo/descricao for contido num arquivo (MIDIA) e for mostrado de maneira correta ou False se ele não estiver contido num arquivo.
     */
    public abstract boolean exibirMidias(String titulo_descricao);

    /**
     * Menu de ordenação para o usuário escolher o tipo de ordem da lista de midias, além de mostrar já ordenado.
     */
    public abstract void ordenar();

}
