package Menus;

import Listas.CrudMidia;
import Validacao.Validacao;
import Midias.Midia;
import java.util.List;

public abstract class MenuMidias implements iMenu {

    CrudMidia crudMidia;
    int opcoes;

    public MenuMidias(CrudMidia crud) {
        this.crudMidia = crud;
    }

    public void menu() {
        boolean retorno = true;
        do {
            System.out.println("Bem vindo ao Menu de Operações");
            System.out.println("O que deseja fazer?");
            System.out.println("1 - Cadastrar");
            System.out.println("2 - Consultar");
            System.out.println("3 - Editar");
            System.out.println("4 - Excluir");
            System.out.println("5 - Ordenar Códigos");
            System.out.println("6 - Salvar Alterações");
            System.out.println("0 - Retornar ao Menu de Mídias");
            opcoes = Validacao.validarInteiro();
            switch (opcoes) {
                case 0:
                    System.out.println("De volta ao menu inicial!");
                    retorno = false;
                    break;
                case 1:
                    cadastrar();
                    break;
                case 2:
                    consultar();
                    break;
                case 3:
                    editar();
                    break;
                case 4:
                    excluir();
                    break;
                case 5:
                    ordenar();
                    break;
                case 6:
                    salvar();
                    break;
            }
        } while (retorno && opcoes != 0);
    }

    @Override
    public abstract void cadastrar();

    @Override
    public abstract void editar();

    @Override
    public void consultar() {
        String titulo;
        System.out.println("Menu de Consulta");
        System.out.println("Qual titulo da midia que deseja consultar?");
        System.out.println("Digite o titulo ou parte da descricao");
        titulo = Validacao.validarString();
        if (!exibirMidias(titulo)) {
            System.out.println("Midia de titulo " + titulo + " inexistentes");
        }
    }

    @Override
    public void excluir() {
        String titulo;
        int codigo;
        System.out.println("Menu de Exclusão");
        System.out.println("Digite o titulo do arquivo que deseja deletar");
        titulo = Validacao.validarString();
        if (exibirMidias(titulo) == true) {
            System.out.println("Digite o código do arquivo a ser excluido");
            codigo = Validacao.validarInteiro();
            if (crudMidia.removeMidia(codigo) == true) {
                System.out.println("Exclusão bem sucedida!");
            } else {
                System.out.println("Código inválido!");
            }
        }
    }

    @Override
    public void salvar() {
        try {
            crudMidia.salvar();
            System.out.println("Salvo com sucesso.");
        } catch (Exception e) {
            System.out.println("Erro no arquivo.");

        }
    }

    @Override
    public boolean exibirMidias(String titulo_descricao) {
        List<Midia> lista = crudMidia.pesquisarMidia(titulo_descricao);
        if (lista == null) {
            System.out.println("Midia inexistente");
            return false;
        } else {
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i));
            }
            return true;

        }
    }

    @Override
    public void ordenar() {
        boolean retorno = true;
        int opcao;
        List<Midia> lista;
        System.out.println("1 - Ordenar por código");
        System.out.println("2 - Ordenar por titulo");
        System.out.println("0 - Voltar ao menu anterior");
        do {
            opcao = Validacao.validarInteiro();
            switch (opcao) {
                case 1:
                    crudMidia.ordenarCodigo();
                    retorno = false;
                    break;
                case 2:
                    crudMidia.ordenarTitulo();
                    retorno = false;
                    break;
                case 0:
                    retorno = false;
                    break;
            }
        } while (retorno && opcao == 0);
        if (opcao != 0) {
            lista = crudMidia.pesquisarTodas();
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i));
            }
        }

    }


}
