package Menus;

import Listas.CrudMidia;
import Midias.Foto;
import Validacao.Validacao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuFoto extends MenuMidias {

    public MenuFoto(CrudMidia crud) {
        super(crud);
    }

    @Override
    public void cadastrar() {
        String fotografo;
        List<String> pessoas = new ArrayList<>();
        String local;
        String descricao;
        String caminhoDoArquivo;
        String titulo;
        String data;
        int escolha;
        double hora;
        System.out.println("Menu de cadastro");
        System.out.println("Titulo da Foto");
        titulo = Validacao.validarString();
        System.out.println("Descrição da Foto: ");
        descricao = Validacao.validarString();
        System.out.println("Caminho da Foto: ");
        caminhoDoArquivo = Validacao.validarString();
        System.out.println("Fotógrafo:");
        fotografo = Validacao.validarString();
        System.out.println("Pessoas contidas na Foto:");
        pessoas.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar mais pessoas?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outra pessoa");
                    pessoas.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);
        System.out.println("Local da Foto:");
        local = Validacao.validarString();
        System.out.println("Data da Foto");
        data = Validacao.validarValidade();
        System.out.println("Hora da Foto:");
        hora = Validacao.validarDouble();
        Foto foto = new Foto(caminhoDoArquivo, titulo, descricao, fotografo, pessoas, local, data, hora);
        if (super.crudMidia.cadastrarMidia(foto) == true) {
            System.out.println("Mídia cadastrada com sucesso!");
        } else {
            System.out.println("Erro no cadastro de mídia!");
        }
    }

    @Override
    public void editar() {
        int escolha;
        Scanner entrada = new Scanner(System.in);
        List<String> pessoas = new ArrayList();
        System.out.println("Qual mídia você deseja editar:");
        String titulo = entrada.nextLine();

        Foto velho = (Foto) super.crudMidia.pesquisarMidia(titulo);
        if (velho != null) {
            System.out.println("Digite o novo caminho:");
            String caminhoDoArquivo = entrada.nextLine();
            if (caminhoDoArquivo.equalsIgnoreCase("")) {
                caminhoDoArquivo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite o novo titulo:");
            titulo = entrada.nextLine();
            if (titulo.equalsIgnoreCase("")) {
                titulo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite a nova descrição:");
            String descricao = entrada.nextLine();
            if (descricao.equalsIgnoreCase("")) {
                descricao = velho.getDescricao();
            }
            System.out.println("Digite o novo fotografo:");
            String fotografo = entrada.nextLine();
            if (fotografo.equalsIgnoreCase("")) {
                fotografo = velho.getFotografo();
            }
            System.out.println("Digite novas pessoas");
            pessoas.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar mais pessoas?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = Validacao.validarInteiro();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione mais pessoas");
                        pessoas.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("Digite o novo local:");
            String local = entrada.nextLine();
            if (local.equalsIgnoreCase("")) {
                local = velho.getLocal();
            }
            System.out.println("data");
            String data = Validacao.validarValidade();
            if (data.equals("")) {
                data = velho.getData();
            }
            System.out.println("Digite o novo ano de lançamento:");
            double hora;
            try {
                hora = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                hora = velho.getHora();
            }
            super.crudMidia.cadastrarMidia(new Foto(caminhoDoArquivo, titulo, descricao, fotografo, pessoas, local, data, hora));

        } else {
            System.out.println("Mídia não encontrada!");
        }
    }

}
