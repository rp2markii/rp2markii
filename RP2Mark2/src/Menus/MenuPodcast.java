package Menus;

import Listas.CrudMidia;
import Midias.Podcast;
import Validacao.Validacao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuPodcast extends MenuMidias {

    public MenuPodcast(CrudMidia crud) {
        super(crud);
    }
    /**
     * cadastra as midias que o usuario quer
     */
    @Override
    public void cadastrar() {
        String idioma;
        List<String> autores = new ArrayList<>();
        String titulo;
        String descricao;
        String caminhoDoArquivo;
        int escolha;
        int ano;
        System.out.println("Menu de cadastro");
        System.out.println("Titulo do Podcast: ");
        titulo = Validacao.validarString();
        System.out.println("Descriçao do Podcast: ");
        descricao = Validacao.validarString();
        System.out.println("Caminho do Podcast: ");
        caminhoDoArquivo = Validacao.validarString();
        System.out.println("Idioma do Podcast:");
        idioma = Validacao.validarString();
        System.out.println("Autores do Podcast:");
        autores.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro autor?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro autor");
                    autores.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);
        System.out.println("Ano do Podcast:");
        ano = Validacao.validarAno();
        Podcast podcast = new Podcast(caminhoDoArquivo, titulo, descricao, idioma, autores, ano);
        if (super.crudMidia.cadastrarMidia(podcast) == true) {
            System.out.println("Partitura cadastrada com sucesso.");
        } else {
            System.out.println("Erro ao cadastrar partitura.");
        }

    }
    /**
     * edita as midias que o usuario desejar
     */
    @Override
    public void editar() {
        int escolha;
        Scanner entrada = new Scanner(System.in);
        List<String> autores = new ArrayList();
        System.out.println("Menu de edição");
        System.out.println("Qual mídia você deseja editar:");
        String titulo = Validacao.validarString();
        
        
        Podcast velho = (Podcast) super.crudMidia.pesquisarMidia(titulo);
        if (velho != null) {
            System.out.println("Digite o novo caminho:");
            String caminhoDoArquivo = entrada.nextLine();
            if (caminhoDoArquivo.equalsIgnoreCase("")) {
                caminhoDoArquivo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite o novo titulo:");
            titulo = entrada.nextLine();
            if (titulo.equalsIgnoreCase("")) {
                titulo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite a nova descrição:");
            String descricao = entrada.nextLine();
            if (descricao.equalsIgnoreCase("")) {
                descricao = velho.getDescricao();
            }
            System.out.println("Digite o novo idioma:");
            String idioma = entrada.nextLine();
            if (idioma.equalsIgnoreCase("")) {
                idioma = velho.getIdioma();
            }
            System.out.println("Digite novos autores");
            autores.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro autor?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = Validacao.validarInteiro();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro autor");
                        autores.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("Digite o novo ano de lançamento:");
            int ano;
            try {
                ano = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                ano = velho.getAno();
            }
            super.crudMidia.cadastrarMidia(new Podcast(caminhoDoArquivo, titulo, descricao, idioma, autores, ano));

        } else {
            System.out.println("Mídia não encontrada!");
        }
    }

}
    


