package Menus;


import Listas.CrudMidia;
import Midias.Ebook;
import Validacao.Validacao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuEbook extends MenuMidias {

    public MenuEbook(CrudMidia crud) {
        super(crud);
    }

    @Override
    public void cadastrar() {
        String genero;
        List<String> autores = new ArrayList<>();
        String local;
        String editora;
        String descricao;
        String caminhoDoArquivo;
        String titulo;
        int escolha;
        int numeroDePaginas;
        int ano;
        System.out.println("Menu de cadastro");
        System.out.println("Titulo do Ebook: ");
        titulo = Validacao.validarString();
        System.out.println("Descriçao do Ebook: ");
        descricao = Validacao.validarString();
        System.out.println("Caminho do Ebook: ");
        caminhoDoArquivo = Validacao.validarString();
        System.out.println("Gênero do Ebook:");
        genero = Validacao.validarString();
        System.out.println("Autor(es) do Ebook:");
        autores.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro autor?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro autor");
                    autores.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);
        System.out.println("Local do Ebook:");
        local = Validacao.validarString();
        System.out.println("Editora do Ebook:");
        editora = Validacao.validarString();
        System.out.println("Numero de páginas do Ebook:");
        numeroDePaginas = Validacao.validarInteiro();
        System.out.println("Ano do Ebook:");
        ano = Validacao.validarAno();
        Ebook ebook = new Ebook(caminhoDoArquivo, titulo, descricao, genero, autores, local, editora, numeroDePaginas, ano);
        if (super.crudMidia.cadastrarMidia(ebook) == true) {
            System.out.println("EBook cadastrado com sucesso.");
        } else {
            System.out.println("Erro ao cadastrar o EBook.");
        }
    }


    @Override
    public void editar() {
        int escolha;
        Scanner entrada = new Scanner(System.in);
        List<String> autores = new ArrayList();
        System.out.println("Menu de edição");
        System.out.println("Qual midia você deseja editar:");
        String tituloBusca = Validacao.validarString();
        
        
        Ebook velho = (Ebook) super.crudMidia.pesquisarMidia(tituloBusca);
        if (velho != null) {
            System.out.println("Digite o novo caminho do arquivo:");
            String caminhoDoArquivo = entrada.nextLine();
            if (caminhoDoArquivo.equalsIgnoreCase("")) {
                caminhoDoArquivo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite o novo titulo:");
            String titulo = entrada.nextLine();
            if (titulo.equalsIgnoreCase("")) {
                titulo = velho.getTitulo();
            }
            System.out.println("Digite a nova descrição:");
            String descricao = entrada.nextLine();
            if (descricao.equalsIgnoreCase("")) {
                descricao = velho.getDescricao();
            }
            System.out.println("Digite o novo gênero:");
            String genero = entrada.nextLine();
            if (genero.equalsIgnoreCase("")) {
                genero = velho.getGenero();
            }
            System.out.println("Digite novos autores");
            autores.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro autor?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = Validacao.validarInteiro();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro autor");
                        autores.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("Digite o novo local:");
            String local = entrada.nextLine();
            if (local.equalsIgnoreCase("")) {
                local = velho.getLocal();
            }
            System.out.println("Digite a nova editora");
            String editora = entrada.nextLine();
            if (editora.equalsIgnoreCase("")) {
                editora = velho.getEditora();
            }
            System.out.println("Digite o novo número de páginas:");
            int numeroDePaginas;
            try {
                numeroDePaginas = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                numeroDePaginas = velho.getNumeroDePaginas();
            }
            System.out.println("Digite o novo ano:");
            int ano;
            try {
                ano = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                ano = velho.getAno();
            }

            super.crudMidia.cadastrarMidia(new Ebook(caminhoDoArquivo, titulo, descricao, genero, autores, local, editora, numeroDePaginas, ano));

        } else {
            System.out.println("Mídia não encontrada!");
        }

    }

    
    }


