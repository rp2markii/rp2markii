package Menus;

import Listas.CrudMidia;
import Midias.PartituraMusical;
import Validacao.Validacao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuPartitura extends MenuMidias {

    public MenuPartitura(CrudMidia crud) {
        super(crud);
    }

    @Override
    public void cadastrar() {
        String genero;
        List<String> instrumentos = new ArrayList<>();
        List<String> autores = new ArrayList<>();
        String descricao;
        String caminhoDoArquivo;
        String titulo;
        int escolha;
        int ano;
        System.out.println("Menu de cadastro");
        System.out.println("Titulo da Partitura Musical: ");
        titulo = Validacao.validarString();
        System.out.println("Descrição da Partitura Musical: ");
        descricao = Validacao.validarString();
        System.out.println("Caminho da Partitura Musical ");
        caminhoDoArquivo = Validacao.validarString();
        System.out.println("Gênero  da Partitura Musical:");
        genero = Validacao.validarString();
        System.out.println("Instrumento usado:");
        instrumentos.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro intrumento?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro intrumento");
                    instrumentos.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);
        System.out.println("Autores da  da Partitura Musical:");
        autores.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro autor?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro autor");
                    autores.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);
        System.out.println("Ano de lançamento:");
        ano = Validacao.validarAno();
        PartituraMusical part = new PartituraMusical(caminhoDoArquivo, titulo, descricao, genero, instrumentos, autores, ano);
        if (super.crudMidia.cadastrarMidia(part) == true) {
            System.out.println("Partitura cadastrada com sucesso.");
        } else {

            System.out.println("Erro ao cadastrar partitura.");
        }

    }

    @Override
    public void editar() {
        int escolha;
        Scanner entrada = new Scanner(System.in);
        List<String> autores = new ArrayList();
        List<String> instrumentos = new ArrayList();
        System.out.println("Menu de edição");
        System.out.println("Qual mídia você deseja editar:");
        String titulo = Validacao.validarString();
       
        
        PartituraMusical velho = (PartituraMusical) super.crudMidia.pesquisarMidia(titulo);
        if (velho != null) {
            System.out.println("Digite o novo titulo:");
            titulo = entrada.nextLine();
            if (titulo.equalsIgnoreCase("")) {
                titulo = velho.getTitulo();
            }
            System.out.println("Digite a nova descrição:");
            String descricao = entrada.nextLine();
            if (descricao.equalsIgnoreCase("")) {
                descricao = velho.getDescricao();
            }
            System.out.println("Digite o novo caminho:");
            String caminhoDoArquivo = entrada.nextLine();
            if (caminhoDoArquivo.equalsIgnoreCase("")) {
                caminhoDoArquivo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite o novo gênero:");
            String genero = entrada.nextLine();
            if (genero.equalsIgnoreCase("")) {
                genero = velho.getGenero();
            }
            System.out.println("Digite novos instrumentos");
            instrumentos.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro instrumento?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = Validacao.validarInteiro();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro instrumento");
                        instrumentos.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("Digite o novo autor:");
            autores.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro autor?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = Validacao.validarInteiro();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro autor:");
                        autores.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("Digite o novo ano de lançamento:");
            int tempAno;
            try {
                tempAno = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                tempAno = velho.getAno();
            }
            super.crudMidia.cadastrarMidia(new PartituraMusical(caminhoDoArquivo, titulo, descricao, genero, instrumentos, autores, tempAno));
        } else {
            System.out.println("Mídia não encontrada!");
        }
    }

  
}

    

