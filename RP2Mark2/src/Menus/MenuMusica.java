package Menus;

import Listas.CrudMidia;
import Midias.Musica;
import Validacao.Validacao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuMusica extends MenuMidias {


    public MenuMusica(CrudMidia crud) {
        super(crud);
    }

    @Override
    public void cadastrar() {
        String genero;
        String idioma;
        List<String> autores = new ArrayList<>();
        List<String> interpretes = new ArrayList<>();
        String descricao;
        String caminhoDoArquivo;
        String titulo;
        double duracao;
        int escolha;
        int ano;
        System.out.println("Menu de cadastro");
        System.out.println("Titulo da Musica: ");
        titulo = Validacao.validarString();
        System.out.println("Descricao da Musica: ");
        descricao = Validacao.validarString();
        System.out.println("Caminho da Musica: ");
        caminhoDoArquivo = Validacao.validarString();
        System.out.println("Gênero da musica:");
        genero = Validacao.validarString();
        System.out.println("Idioma da musica:");
        idioma = Validacao.validarString();
        System.out.println("Autor(es) da musica:");
        autores.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro autor?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro autor");
                    autores.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);
        System.out.println("Interpretes da musica:");
        interpretes.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro interprete?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro interprete");
                    interpretes.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);

        System.out.println("Duração da musica:");
        duracao = Validacao.validarDouble();
        System.out.println("Ano de lançamento:");
        ano = Validacao.validarAno();
        Musica musica = new Musica(caminhoDoArquivo, titulo, descricao, genero, idioma, autores, interpretes, duracao, ano);
        if (super.crudMidia.cadastrarMidia(musica) == true) {
            System.out.println("Cadastro efetuado com sucesso.");
        } else {
            System.out.println("Erro ao cadastrar o arquivo.");
        }

    }

    @Override
    public void editar() {
        int escolha;
        Scanner entrada = new Scanner(System.in);
        List<String> autores = new ArrayList<>();
        List<String> interpretes = new ArrayList<>();
       
        
        System.out.println("Menu de edição");
        System.out.println("Qual mídia você deseja editar:");
        String titulo = Validacao.validarString();
        Musica velho = (Musica) super.crudMidia.pesquisarMidia(titulo);
        if (velho != null) {
            System.out.println("Digite o novo caminho:");
            String caminhoDoArquivo = entrada.nextLine();
            if (caminhoDoArquivo.equalsIgnoreCase("")) {
                caminhoDoArquivo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite o novo titulo:");
            titulo = entrada.nextLine();
            if (titulo.equalsIgnoreCase("")) {
                titulo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite a nova descrição:");
            String descricao = entrada.nextLine();
            if (descricao.equalsIgnoreCase("")) {
                descricao = velho.getDescricao();
            }
            System.out.println("Digite o novo gênero:");
            String genero = entrada.nextLine();
            if (genero.equalsIgnoreCase("")) {
                genero = velho.getGenero();
            }
            System.out.println("Digite o novo idioma:");
            String idioma = entrada.nextLine();
            if (idioma.equalsIgnoreCase("")) {
                idioma = velho.getIdioma();
            }
            System.out.println("Digite novos autores");
            autores.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro autor?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = Validacao.validarInteiro();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro autor");
                        autores.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("Digite novos interpretes");
            interpretes.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro interprete?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = entrada.nextInt();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro interprete");
                        interpretes.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("duração");
            double duracao;
            try {
                duracao = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                duracao = velho.getDuracao();
            }
            System.out.println("Digite o novo ano de lançamento:");
            int ano;
            try {
                ano = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                ano = velho.getAno();
            }
            super.crudMidia.cadastrarMidia(new Musica(caminhoDoArquivo, titulo, descricao, genero, idioma, autores, interpretes, duracao, ano));

        } else {
            System.out.println("Mídia não encontrada!");
        }
    }

   
}
