package Menus;

import Listas.CrudMidia;
import Midias.Jogo;
import Validacao.Validacao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuJogos extends MenuMidias {

    public MenuJogos(CrudMidia crud) {
        super(crud);
    }

    @Override
    public void cadastrar() {
        String genero;
        List<String> autores = new ArrayList<>();
        String descricao;
        String caminhoDoArquivo;
        String titulo;
        int numeroDeJogadores;
        int escolha;
        int ano;
        boolean suporteARede = false;
        System.out.println("Menu de cadastro");
        System.out.println("Titulo do Jogo: ");
        titulo = Validacao.validarString();
        System.out.println("Descrição do Jogo: ");
        descricao = Validacao.validarString();
        System.out.println("Caminho do Jogo: ");
        caminhoDoArquivo = Validacao.validarString();
        System.out.println("Gênero do Jogo: ");
        genero = Validacao.validarString();
        System.out.println("Autor(es) do Jogo: ");
        autores.add(Validacao.validarString());
        do {
            System.out.println("Deseja adicionar um outro autor?");
            System.out.println("1 - Sim");
            System.out.println("2 - Não");
            escolha = Validacao.validarInteiro();
            switch (escolha) {
                case 1:
                    System.out.println("Adicione outro autor");
                    autores.add(Validacao.validarString());
                    break;
                case 2:
                    break;
            }
        } while (escolha != 2);
        System.out.println("Numero de jogadores: ");
        numeroDeJogadores = Validacao.validarInteiro();
        System.out.println("Suporte a rede online");
        System.out.println("1 - Sim");
        System.out.println("2 - Não");
        escolha = Validacao.validarInteiro();
        switch (escolha) {
            case 1:
                suporteARede = true;
                break;
            case 2:
                suporteARede = false;
                break;
        }
        System.out.println("Ano de lançamento:");
        ano = Validacao.validarAno();
        Jogo jogo = new Jogo(caminhoDoArquivo, titulo, descricao, genero, autores, numeroDeJogadores, suporteARede, ano);
        if (super.crudMidia.cadastrarMidia(jogo) == true) {
            System.out.println("Mídia cadastrada com sucesso!");
        } else {
            System.out.println("Erro no cadastro de mídia!");
        }

    }

    @Override
    public void editar() {
        int escolha;
        Scanner entrada = new Scanner(System.in);
        List<String> autores = new ArrayList();
        System.out.println("Menu de edição");
        System.out.println("Qual mídia você deseja editar:");
        String titulo = entrada.nextLine();
        
        
        Jogo velho = (Jogo) super.crudMidia.pesquisarMidia(titulo);
        if (velho != null) {
            System.out.println("Digite o novo caminho:");
            String caminhoDoArquivo = entrada.nextLine();
            if (caminhoDoArquivo.equalsIgnoreCase("")) {
                caminhoDoArquivo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite o novo titulo:");
            titulo = entrada.nextLine();
            if (titulo.equalsIgnoreCase("")) {
                titulo = velho.getCaminhoDoArquivo();
            }
            System.out.println("Digite a nova descrição:");
            String descricao = entrada.nextLine();
            if (descricao.equalsIgnoreCase("")) {
                descricao = velho.getDescricao();
            }
            System.out.println("Digite o novo gênero:");
            String genero = entrada.nextLine();
            if (genero.equalsIgnoreCase("")) {
                genero = velho.getGenero();
            }
            System.out.println("Digite novos autores");
            autores.add(Validacao.validarString());
            do {
                System.out.println("Deseja adicionar um outro autor?");
                System.out.println("1 - Sim");
                System.out.println("2 - Não");
                escolha = Validacao.validarInteiro();
                switch (escolha) {
                    case 1:
                        System.out.println("Adicione outro autor");
                        autores.add(Validacao.validarString());
                        break;
                    case 2:
                        break;
                }
            } while (escolha != 2);
            System.out.println("Digite o novo ano de lançamento:");
            int numeroDeJogadores;
            try {
                numeroDeJogadores = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                numeroDeJogadores = velho.getAno();
            }
            System.out.println("Digite o novo diretor:");
            boolean suporteARede = entrada.nextBoolean();
            //if (suporteARede.equalsIgnoreCase("")) {
            //     suporteARede = velho.getSuporteARede();
            // }
            System.out.println("Digite o novo ano de lançamento:");
            int ano;
            try {
                ano = Integer.parseInt(entrada.nextLine());
            } catch (NumberFormatException e) {
                ano = velho.getAno();
            }
            super.crudMidia.cadastrarMidia(new Jogo(caminhoDoArquivo, titulo, descricao, genero, autores, numeroDeJogadores, suporteARede, ano));

        } else {
            System.out.println("Mídia não encontrada!");
        }

    }
}
