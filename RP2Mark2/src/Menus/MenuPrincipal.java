package Menus;

import Listas.CrudAudiolivro;
import Listas.CrudEbook;
import Listas.CrudFilme;
import Listas.CrudFoto;
import Listas.CrudJogos;
import Listas.CrudMidia;
import Listas.CrudMusica;
import Listas.CrudPartMusical;
import Listas.CrudPodcast;
import Midias.Midia;
import Validacao.Validacao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MenuPrincipal {

    CrudMidia lista_jogos = new CrudJogos(new ArrayList(), "Jogos.txt");
    CrudMidia lista_filmes = new CrudFilme(new ArrayList(), "Filmes.txt");
    CrudMidia lista_fotos = new CrudFoto(new ArrayList(), "Fotos.txt");
    CrudMidia lista_ebook = new CrudEbook(new ArrayList(), "Ebooks.txt");
    CrudMidia lista_audiolivro = new CrudAudiolivro(new ArrayList(), "Audiolivro.txt");
    CrudMidia lista_musica = new CrudMusica(new ArrayList(), "Musica.txt");
    CrudMidia lista_part = new CrudPartMusical(new ArrayList(), "Partitura Musical.txt");
    CrudMidia lista_podcast = new CrudPodcast(new ArrayList(), "Podcast.txt");
    MenuMidias menuMidias;

    public void menuPrincipal() {
        inicializar();
        int opcoes;
        do {
            System.out.println("Bem vindo ao Menu de Midias!");
            System.out.println("Que tipo de mídia deseja operar");
            System.out.println("1 - Audiolivro");
            System.out.println("2 - Ebook");
            System.out.println("3 - Filme");
            System.out.println("4 - Foto");
            System.out.println("5 - Jogo");
            System.out.println("6 - Musica");
            System.out.println("7 - Podcast");
            System.out.println("8 - Partitura Musical");
            System.out.println("9 - Pesquisar em todas as midias");
            System.out.println("0 - Sair");
            opcoes = Validacao.validarInteiro();
            switch (opcoes) {
                case 0:
                    System.out.println("Até mais!");
                    break;
                case 1:
                    menuMidias = new MenuAudioLivro(lista_audiolivro);
                    menuMidias.menu();
                    break;
                case 2:
                    menuMidias = new MenuEbook(lista_ebook);
                    menuMidias.menu();
                    break;
                case 3:
                    menuMidias = new MenuFilme(lista_filmes);
                    menuMidias.menu();
                    break;
                case 4:
                    menuMidias = new MenuFoto(lista_fotos);
                    menuMidias.menu();
                    break;
                case 5:
                    menuMidias = new MenuJogos(lista_jogos);
                    menuMidias.menu();
                case 6:
                    menuMidias = new MenuMusica(lista_musica);
                    menuMidias.menu();
                    break;
                case 7:
                    menuMidias = new MenuPodcast(lista_podcast);
                    menuMidias.menu();
                    break;
                case 8:
                    menuMidias = new MenuPartitura(lista_part);
                    menuMidias.menu();
                    break;
                case 9:
                    pesquisarTodas();
                    break;
                default:
                    System.err.println("Opção inválida.");
                    break;
            }
        } while (opcoes != 0);
    }

    /**
     * Exibe as midias
     * @param lista
     * @param titulo
     */
    public void exibirMidias(List <CrudMidia> lista, String titulo) {
        List<Midia> midias;
            for (int i = 0; i < lista.size(); i++) {
               midias = lista.get(i).pesquisarMidia(titulo);
                for (Midia midiaV : midias) {
                    System.out.println(midiaV);
            }
        }
    }

    /**
     * Carrega todas listas e chama o metodo exibirMidias
     */
    public void pesquisarTodas() {
        CrudMidia lista[] = {lista_audiolivro, lista_ebook, lista_filmes, lista_fotos, lista_jogos, lista_musica, lista_podcast, lista_part};
        List <CrudMidia> listaV = new ArrayList<>();
        listaV.addAll(Arrays.asList(lista));
        System.out.println("Digite o título de seu interesse");
        String titulo = Validacao.validarString();
        exibirMidias(listaV, titulo);

    }

    /**
     * Carrega o sistema de listas
     */
    public void inicializar() {
        CrudMidia lista[] = {lista_audiolivro, lista_ebook, lista_filmes, lista_fotos, lista_jogos, lista_musica, lista_podcast, lista_part};
        Validacao.inicializar(lista);
    }

}
