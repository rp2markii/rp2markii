package Listas;

import Midias.Musica;
import Midias.Midia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrudMusica extends CrudMidia {

    public CrudMusica(List midia, String nomeArquivo) {
        super(midia, nomeArquivo);
    }

    @Override
    public void ordenarCodigo() {
        boolean houveTroca;
        do {
            houveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareTo(midias.get(i+1).getTitulo()) < 0 ) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i+1));
                    midias.set(i+1, temp);
                    houveTroca = true;
                }
            }
        } while (houveTroca);
    }

    @Override
    public void carregar() throws Exception {
        File arquivo = new File(nomeArquivo);
        if (arquivo.exists()) {
            List<String> autores = new ArrayList<>();
            List <String> interpretes = new ArrayList<>();
            FileInputStream inFile;
            BufferedReader buff;
            String linha;
            String idioma;
            String caminhoDoArquivo;
            String titulo;
            String descricao;
            String genero;
            String linhaEmBranco;
            Musica musica;
            int numeroMusicas;
            int codigo;
            int ano;
            double duracao;
            


            inFile = new FileInputStream(arquivo);
            buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            linha = buff.readLine();
            numeroMusicas = Integer.parseInt(linha);
            for (int i = 0; i < numeroMusicas; i++) {
                linha = buff.readLine();
                codigo = Integer.parseInt(linha);
                caminhoDoArquivo = buff.readLine();
                titulo = buff.readLine();
                descricao = buff.readLine();
                genero = buff.readLine();
                idioma = buff.readLine();
                linha = buff.readLine();
                autores.addAll(Arrays.asList(linha.split(";")));
                linha = buff.readLine();
                interpretes.addAll(Arrays.asList(linha.split(";")));
                linha = buff.readLine();
                duracao = Double.parseDouble(linha);
                linha = buff.readLine();
                ano = Integer.parseInt(linha);
                linhaEmBranco = buff.readLine();
                musica = new Musica(caminhoDoArquivo, titulo, descricao, genero, idioma, new ArrayList(autores), new ArrayList(interpretes), duracao, ano);
                musica.setCodigo(codigo);
                midias.add(musica);

                autores.clear();
                interpretes.clear();
            }
            buff.close();
            inFile.close();
        }
        else {
            throw new Exception("Arquivo não existente");
        }
    }

    @Override
    public void ordenarTitulo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
