package Listas;

import Midias.Filme;
import Midias.Midia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrudFilme extends CrudMidia {

    public CrudFilme(List midia, String nomeArquivo) {
        super(midia, nomeArquivo);
    }

    @Override
    public void ordenarCodigo() {
        boolean houveTroca;
        do {
            houveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareTo(midias.get(i + 1).getTitulo()) < 0) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i + 1));
                    midias.set(i + 1, temp);
                    houveTroca = true;
                }
            }
        } while (houveTroca);
    }

    @Override
    public void carregar() throws Exception {
        
        File arquivo = new File(nomeArquivo);
        if (arquivo.exists()) {
            List <String> atoresPrincipais = new ArrayList<>();
            FileInputStream inFile;
            BufferedReader buff;
            Filme filme;
            String linha;
            String caminhoDoArquivo;
            String titulo;
            String descricao;
            String genero;
            String diretor;
            String linhaEmBranco;
            String idioma;
            String local;
            String editora;
            int numeroFilmes;
            int codigo;
            int ano;
            double duracao;
            
            
            inFile = new FileInputStream(arquivo);
            buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            linha = buff.readLine();
            numeroFilmes = Integer.parseInt(linha);
            for (int i = 0; i < numeroFilmes; i++) {
                linha = buff.readLine();
                codigo = Integer.parseInt(linha);
                caminhoDoArquivo = buff.readLine();
                titulo = buff.readLine();
                descricao = buff.readLine();
                genero = buff.readLine();
                idioma = buff.readLine();
                diretor = buff.readLine();
                linha = buff.readLine();
                atoresPrincipais.addAll(Arrays.asList(linha.split(";")));
                linha = buff.readLine();
                duracao = Double.parseDouble(linha);
                linha = buff.readLine();
                ano = Integer.parseInt(linha);
                linhaEmBranco = buff.readLine();
                filme = new Filme(caminhoDoArquivo, titulo, descricao, genero, idioma, diretor, new ArrayList(atoresPrincipais), duracao, ano);
                filme.setCodigo(codigo);
                midias.add(filme);
                atoresPrincipais.clear();
            }
            buff.close();
            inFile.close();

        }
        else {
            throw new Exception("Arquivo não existente");
        }

    }

    @Override
    public void ordenarTitulo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
