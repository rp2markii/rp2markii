package Listas;

import Midias.Midia;
import Midias.Podcast;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrudPodcast extends CrudMidia {

    public CrudPodcast(List midia, String nomeArquivo) {
        super(midia, nomeArquivo);
    }

    /**
     * Ordena pelo codigo da midia
     */
    @Override
    public void ordenarCodigo() {
        Midia temp;
        for (int i = 0; i < midias.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (midias.get(i).getCODIGO() > midias.get(i).getCODIGO()) {
                    temp = midias.get(i);
                    midias.set(i, midias.get(j));
                    midias.set(j, temp);

                }
            }
        }

    }

    /**
     * Ordena por titulo da midia
     */
    @Override
    public void ordenarTitulo() {
        boolean HouveTroca;
        do {

            HouveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareToIgnoreCase(midias.get(i + 1).getTitulo()) >= 1) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i + 1));
                    midias.set(i + 1, temp);
                    HouveTroca = true;
                }
            }
        } while (HouveTroca);

    }

    /**
     * grava os metodos do usuario em uma memoria secundaria
     *
     * @throws Exception
     */
    @Override
    public void carregar() throws Exception {
        File arquivo = new File(nomeArquivo);
        if (arquivo.exists()) {
            List<String> autores = new ArrayList<>();
            FileInputStream inFile;
            BufferedReader buff;
            Podcast podcast;
            String linha;
            String linhaEmBranco;
            String caminhoDoArquivo;
            String titulo;
            String descricao;
            String idioma;
            int codigo;
            int autoresNum;
            int ano;
            int numeroPodcast;

            inFile = new FileInputStream(arquivo);
            buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            linha = buff.readLine();
            numeroPodcast = Integer.parseInt(linha);
            for (int i = 0; i < numeroPodcast; i++) {
                linha = buff.readLine();
                codigo = Integer.parseInt(linha);
                caminhoDoArquivo = buff.readLine();
                titulo = buff.readLine();
                descricao = buff.readLine();
                idioma = buff.readLine();
                linha = buff.readLine();
                autores.addAll(Arrays.asList(linha.split(";")));
                linha = buff.readLine();
                ano = Integer.parseInt(linha);
                linhaEmBranco = buff.readLine();
                podcast = new Podcast(caminhoDoArquivo, titulo, descricao, idioma, new ArrayList(autores), ano);
                podcast.setCodigo(codigo);
                midias.add(podcast);
                autores.clear();
            }
            buff.close();
            inFile.close();
        } else {
            throw new Exception("Arquivo não existente");
        }
    }

}
