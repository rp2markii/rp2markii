/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listas;

import Midias.Audiolivro;
import Midias.Midia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrudAudiolivro extends CrudMidia {

    public CrudAudiolivro(List midia, String nomeArquivo) {

        super(midia, nomeArquivo);
    }

    /**
     * Ordena por titulo a midia de Podcasts
     */
    @Override
    public void ordenarCodigo() {
        Midia temp;
        for (int i = 0; i < midias.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (midias.get(i).getCODIGO() > midias.get(i+1).getCODIGO()) {
                    temp = midias.get(i);
                    midias.set(i, midias.get(j));
                    midias.set(j, temp);

                }
            }
        }

    }

    /**
     * Ordena a midia por titulo
     */
    @Override
    public void ordenarTitulo() {
        boolean HouveTroca;
        do {
            HouveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareToIgnoreCase(midias.get(i + 1).getTitulo())>= 1) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i + 1));
                    midias.set(i + 1, temp);
                    HouveTroca = true;
                }
            }
        } while (HouveTroca);

    }

    /**
     * o metodo grava os dados para não perder ao reiniciar o programa
     *
     * @throws Exception
     */
    @Override
    public void carregar() throws Exception {
        File arquivo = new File(nomeArquivo);
        if (arquivo.exists()) {
            List<String> autores = new ArrayList<>();
            FileInputStream inFile;
            BufferedReader buff;
            Audiolivro audiolivro;
            String linha;
            String caminhoDoArquivo;
            String titulo;
            String descricao;
            String genero;
            String linhaEmBranco;
            String idioma;
            String local;
            String editora;
            int numeroAudiolivro;
            int codigo;
            int ano;
            double duracao;
            
            
            inFile = new FileInputStream(arquivo);
            buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            linha = buff.readLine();
            numeroAudiolivro = Integer.parseInt(linha);
            for (int i = 0; i < numeroAudiolivro; i++) {
                linha = buff.readLine();
                codigo = Integer.parseInt(linha);
                caminhoDoArquivo = buff.readLine();
                titulo = buff.readLine();
                descricao = buff.readLine();
                genero = buff.readLine();
                idioma = buff.readLine();
                linha = buff.readLine();
                autores.addAll(Arrays.asList(linha.split(";")));
                local = buff.readLine();
                editora = buff.readLine();
                linha = buff.readLine();
                duracao = Double.parseDouble(linha);
                linha = buff.readLine();
                ano = Integer.parseInt(linha);
                linhaEmBranco = buff.readLine();
                audiolivro = new Audiolivro(caminhoDoArquivo, titulo, descricao, genero, idioma, new ArrayList(autores), local, editora, duracao, ano);
                audiolivro.setCodigo(codigo);
                midias.add(audiolivro);
                autores.clear();
            }
            buff.close();
            inFile.close();

        }
        else {
            throw new Exception("Arquivo não existente");
        }
    }
}
