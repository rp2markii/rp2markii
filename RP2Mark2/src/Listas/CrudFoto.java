package Listas;

import Midias.Foto;
import Midias.Midia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrudFoto extends CrudMidia {

    public CrudFoto(List midia, String nomeArquivo) {
        super(midia, nomeArquivo);
    }

    @Override
    public void ordenarCodigo() {
        for (int ponteiro = 0; ponteiro < midias.size() - 1; ponteiro++) {
            int minimo = ponteiro;
            for (int i = ponteiro + 1; i < midias.size(); i++) {
                if (midias.get(i).getCODIGO() > midias.get(i+1).getCODIGO()) {
                    minimo = i;
                }
            }
            Midia temp = midias.get(ponteiro);
            midias.set(ponteiro, midias.get(minimo));
            midias.set(minimo, temp);
        }

    }
    @Override
    public void ordenarTitulo() {
        for (int ponteiro = 0; ponteiro < midias.size() - 1; ponteiro++) {
            int minimo = ponteiro;
            for (int i = ponteiro + 1; i < midias.size(); i++) {
                if (midias.get(i).getTitulo().compareToIgnoreCase(midias.get(i + 1).getTitulo()) >= 1) {
                    minimo = i;
                }
            }
            Midia temp = midias.get(ponteiro);
            midias.set(ponteiro, midias.get(minimo));
            midias.set(minimo, temp);
        }

    }

    @Override
    public void carregar() throws Exception {
        File arquivo = new File(nomeArquivo);
        if (arquivo.exists()) {
            List<String> pessoas = new ArrayList<>();
            FileInputStream inFile;
            BufferedReader buff;
            Foto foto;
            String linha;
            String linhaEmBranco;
            String data;
            String local;
            String caminhoDoArquivo;
            String titulo;
            String descricao;
            String fotografo;
            int numPessoas;
            int codigo;
            int numeroDeFotos;
            double hora;
            
            
            inFile = new FileInputStream(arquivo);
            buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            linha = buff.readLine();
            numeroDeFotos = Integer.parseInt(linha);
            for (int i = 0; i < numeroDeFotos; i++) {
                linha = buff.readLine();
                codigo = Integer.parseInt(linha);
                caminhoDoArquivo = buff.readLine();
                titulo = buff.readLine();
                descricao = buff.readLine();
                fotografo = buff.readLine();
                linha = buff.readLine();
                pessoas.addAll(Arrays.asList(linha.split(";")));
                local = buff.readLine();
                data = buff.readLine();
                linha = buff.readLine();
                hora = Double.parseDouble(linha);
                linhaEmBranco = buff.readLine();
                foto = new Foto(caminhoDoArquivo, titulo, descricao, fotografo, new ArrayList(pessoas), local, data, hora);
                foto.setCodigo(codigo);
                midias.add(foto);
                pessoas.clear();
            }
            buff.close();
            inFile.close();
        }
        else {
            throw new Exception("Arquivo não existente");
        }
        
    }
}
