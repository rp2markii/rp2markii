package Listas;

import Midias.Midia;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;


public abstract class CrudMidia implements iMidia {

    String nomeArquivo;
    List<Midia> midias;

    public CrudMidia(List midia, String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
        this.midias = midia;
    }

    @Override
    public boolean cadastrarMidia(Midia m) {
        if (m != null) {
            return midias.add(m);
        }
        return false;
    }

    @Override
    public List<Midia> pesquisarMidia(String titulo) {
        List<Midia> lista = new ArrayList<>();
        for (int i = 0; i < midias.size(); i++) {
            if (midias.get(i).getTitulo().toUpperCase().contains(titulo.toUpperCase()) || midias.get(i).getDescricao().toUpperCase().contains(titulo.toUpperCase())) {
                lista.add(midias.get(i));
            }
        }
        return lista;

    }
    @Override
    public List<Midia> pesquisarTodas(){
        return midias;
    }
    @Override
    public boolean removeMidia(int codigo) {
        for (int i = 0; i < midias.size(); i++) {
            if (midias.get(i).getCODIGO() == codigo) {
                midias.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean editarMidia(Midia antiga, Midia nova) {
        for (int i = 0; i < midias.size(); i++) {
            if (midias.get(i).equals(antiga)) {
                midias.set(i, nova);
                return true;
            }

        }
        return false;
    }

    @Override
    public void salvar() throws Exception {
        FileOutputStream outFile;
        BufferedWriter buff;
        outFile = new FileOutputStream(new File(nomeArquivo));
        buff = new BufferedWriter(new OutputStreamWriter(outFile, "UTF-8"));
        buff.write(midias.size() + "\r\n");
        for (Midia midia : midias) {
            buff.write(midia.toFile() + "\r\n");
        }
        buff.close();
        outFile.close();
    }

    @Override
    public abstract void ordenarCodigo();

    @Override
    public abstract void carregar() throws Exception;
    
    @Override
    public abstract void ordenarTitulo();
}
