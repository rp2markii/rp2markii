package Listas;

import Midias.Midia;
import java.util.List;

public interface iMidia {

    /**
     * Casdastra uma midia nova adicionando ela a lista de midias.
     * @param m
     * @return True se o cadastro foi bem sucedido ou False se ocorreu um erro. 
     */
    public abstract boolean cadastrarMidia(Midia m);

    /**
     * Pesquisa a midia pelo titulo.
     * @param titulo
     * @return Lista com todas as midias que contenham o parametro. 
     */
    public abstract List<Midia> pesquisarMidia(String titulo);
    
    /**
     * Lança toda lista de midia de uma determinada midia (Jogo, filme, etc...)
     * @return Lista de toda midia que for requerida, sendo ordenada ou não.
     */
    public abstract List<Midia> pesquisarTodas();

    /**
     * Metodo que permite a exclusão de uma mídia pelo seu código
     * @param numero_escolhido
     * @return True se foi possivel a exclusão da midia ou False se ocorreu um erro.
     */
    public abstract boolean removeMidia(int numero_escolhido);

    /**
     *
     * @param antiga
     * @param nova
     * @return
     */
    public abstract boolean editarMidia(Midia antiga, Midia nova);

    /**
     * Salva em um arquivo TXT a lista de Midias acessada no momento.
     * @throws Exception
     */
    public abstract void salvar() throws Exception;
    /**
     * Ordena mídias em ordem de menor código ao maior
     */
    public abstract void ordenarCodigo();
    
    /**
     * Ordena mídias por título em ordem alfabetica
     */
    public abstract void ordenarTitulo();

     /**
     * Le linha por linha arquivo diretamento do .TXT salvo pelo programa
     * @throws Exception
     */
    public abstract void carregar() throws Exception;
    

}
