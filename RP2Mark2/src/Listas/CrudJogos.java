package Listas;

import Midias.Jogo;
import Midias.Midia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrudJogos extends CrudMidia {

    public CrudJogos(List midia, String nomeArquivo) {
        super(midia, nomeArquivo);
    }

    
    @Override
    public void ordenarCodigo() {
        boolean HouveTroca;
        do {
            HouveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getCODIGO() > midias.get(i+1).getCODIGO()) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i + 1));
                    midias.set(i + 1, temp);
                    HouveTroca = true;
                }
            }
        } while (HouveTroca);
    }

   
    @Override
    public void ordenarTitulo() {
        boolean HouveTroca;
        do {
            HouveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareToIgnoreCase(midias.get(i + 1).getTitulo()) >= 1) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i + 1));
                    midias.set(i + 1, temp);
                    HouveTroca = true;
                }
            }
        } while (HouveTroca);

    }

   
    @Override
    public void carregar() throws Exception {
        File arquivo = new File(nomeArquivo);
        if (arquivo.exists()) {
            List<String> autores = new ArrayList<>();
            FileInputStream inFile;
            BufferedReader buff;
            Jogo jogo;
            String linha;
            String caminhoDoArquivo;
            String titulo;
            String descricao;
            String genero;
            String linhaEmBranco;
            int numeroJogos;
            int codigo;
            int numeroDeJogadores;
            int ano;
            boolean suporteARede;
            

            inFile = new FileInputStream(arquivo);
            buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            linha = buff.readLine();
            numeroJogos = Integer.parseInt(linha);
            for (int i = 0; i < numeroJogos; i++) {
                linha = buff.readLine();
                codigo = Integer.parseInt(linha);
                caminhoDoArquivo = buff.readLine();
                titulo = buff.readLine();
                descricao = buff.readLine();
                genero = buff.readLine();
                linha = buff.readLine();
                numeroDeJogadores = Integer.parseInt(linha);
                linha = buff.readLine();
                autores.addAll(Arrays.asList(linha.split(";")));
                linha = buff.readLine();
                suporteARede = Boolean.parseBoolean(linha);
                linha = buff.readLine();
                ano = Integer.parseInt(linha);
                linhaEmBranco = buff.readLine();
                jogo = new Jogo(caminhoDoArquivo, titulo, descricao, genero, new ArrayList(autores), numeroDeJogadores, suporteARede, ano);
                jogo.setCodigo(codigo);
                midias.add(jogo);

                autores.clear();
            }
            buff.close();
            inFile.close();
        }
        else {
            throw new Exception("Arquivo não existente");
        }
    }

}
