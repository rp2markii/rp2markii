package Listas;

import Midias.Midia;
import Midias.PartituraMusical;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrudPartMusical extends CrudMidia {

    public CrudPartMusical(List midia, String nomeArquivo) {
        super(midia, nomeArquivo);
    }

    @Override
    public void ordenarCodigo() {
        boolean houveTroca;
        do {
            houveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareTo(midias.get(i + 1).getTitulo()) < 0) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i + 1));
                    midias.set(i + 1, temp);
                    houveTroca = true;
                }
            }
        } while (houveTroca);

    }

    @Override
    public void ordenarTitulo() {
        boolean HouveTroca;
        do {
            HouveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareToIgnoreCase(midias.get(i + 1).getTitulo())
                        >= 1) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i + 1));
                    midias.set(i + 1, temp);
                    HouveTroca = true;
                }
            }
        } while (HouveTroca);

    }

    @Override
    public void carregar() throws Exception {
        File arquivo = new File(nomeArquivo);
        if (arquivo.exists()) {
            List<String> instrumentos = new ArrayList<>();
            List<String> autores = new ArrayList<>();
            FileInputStream inFile;
            BufferedReader buff;
            PartituraMusical partituraMusical;
            String linha;
            String linhaEmBranco;
            String caminhoDoArquivo;
            String titulo;
            String descricao;
            String genero;
            int numeroPartituras;
            int codigo;
            int ano;

            inFile = new FileInputStream(arquivo);
            buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            linha = buff.readLine();
            numeroPartituras = Integer.parseInt(linha);
            for (int i = 0; i < numeroPartituras; i++) {
                linha = buff.readLine();
                codigo = Integer.parseInt(linha);
                caminhoDoArquivo = buff.readLine();
                titulo = buff.readLine();
                descricao = buff.readLine();
                genero = buff.readLine();
                linha = buff.readLine();
                autores.addAll(Arrays.asList(linha.split(";")));
                linha = buff.readLine();
                instrumentos.addAll(Arrays.asList(linha.split(";")));
                linha = buff.readLine();
                ano = Integer.parseInt(linha);
                linhaEmBranco = buff.readLine();
                partituraMusical = new PartituraMusical(caminhoDoArquivo, titulo, descricao, genero, new ArrayList(instrumentos), new ArrayList(autores), ano);
                partituraMusical.setCodigo(codigo);
                midias.add(partituraMusical);
                autores.clear();
                instrumentos.clear();
            }
            buff.close();
            inFile.close();
        }
        else {
            throw new Exception("Arquivo não existente");
        }
    }

}
