package Listas;

import Midias.Ebook;
import Midias.Midia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;

public class CrudEbook extends CrudMidia {

    public CrudEbook(List midia, String nomeArquivo) {
        super(midia, nomeArquivo);
    }

    @Override
    public void ordenarCodigo() {
        boolean houveTroca;
        do {
            houveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareTo(midias.get(i+1).getTitulo()) < 0 ) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i+1));
                    midias.set(i+1, temp);
                    houveTroca = true;
                }
            }
        } while (houveTroca);
    }
    @Override
    public void ordenarTitulo(){
        boolean HouveTroca;
        do {
            HouveTroca = false;
            for (int i = 0; i < midias.size() - 1; i++) {
                if (midias.get(i).getTitulo().compareToIgnoreCase(midias.get(i + 1).getTitulo()) >= 1) {
                    Midia temp = midias.get(i);
                    midias.set(i, midias.get(i + 1));
                    midias.set(i + 1, temp);
                    HouveTroca = true;
                }
            }
        } while (HouveTroca);
        

    }

    @Override
    public void carregar() throws Exception {
        File arquivo = new File(nomeArquivo);
        if (arquivo.exists()) {
        FileInputStream inFile;
        BufferedReader buff;
        String linha;
        String linhaEmBranco;
        int numeroJogos;
        Ebook ebook;
        int codigo;
        String caminhoDoArquivo;
        String titulo;
        String descricao;
        String genero;
        List<String> autores = new ArrayList<>();
        int numAutores;
        String editora;
        String local;
        int numeroDePaginas;
        int ano;
        
        
        inFile = new FileInputStream(arquivo);
        buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
        linha = buff.readLine();
        numeroJogos = Integer.parseInt(linha);
        for (int i = 0; i < numeroJogos; i++) {
            linha = buff.readLine();
            codigo = Integer.parseInt(linha);
            caminhoDoArquivo = buff.readLine();
            titulo = buff.readLine();
            descricao = buff.readLine();
            genero = buff.readLine();
            linha = buff.readLine();
            autores.addAll(Arrays.asList(linha.split(";")));
            local = buff.readLine();
            editora = buff.readLine();
            linha = buff.readLine();
            numeroDePaginas = Integer.parseInt(linha);
            linha = buff.readLine();
            ano = Integer.parseInt(linha);
            linhaEmBranco = buff.readLine();
            ebook = new Ebook(caminhoDoArquivo, titulo, descricao, genero, autores, local, editora, numeroDePaginas, ano);
            ebook.setCodigo(codigo);
            midias.add(ebook);
        }
        buff.close();
        inFile.close();

        }
        else {
            throw new Exception("Arquivo não existente");
        }
    }
}
