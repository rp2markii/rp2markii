package Midias;

import java.util.List;

public class Audiolivro extends Midia {

    private String genero;
    private String idioma;
    private List autores;
    private String local;
    private String editora;
    private double duracao;
    private int ano;

    public Audiolivro(String caminhoDoArquivo, String titulo, String descricao, String genero, String idioma, List autores, String local, String editora, double duracao, int ano) {
        super(caminhoDoArquivo, titulo, descricao);
        this.genero = genero;
        this.idioma = idioma;
        this.autores = autores;
        this.local = local;
        this.editora = editora;
        this.duracao = duracao;
        this.ano = ano;

    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getGenero() {
        return genero;
    }

    public void setIdioma(String idiomas) {
        this.idioma = idiomas;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setAutores(List autores) {
        this.autores = autores;
    }

    public List getAutores() {
        return autores;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getLocal() {
        return local;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getEditora() {
        return editora;
    }

    public void setDuracao(double duracao) {
        this.duracao = duracao;
    }

    public double getDuracao() {
        return duracao;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getAno() {
        return ano;
    }

    @Override
    public String toString() {
        String dados = super.toString() + "\n" + "Genero: " + genero + "\n" + "Idiomas: " + idioma + "\n" + "Autores: ";
        for (int i = 0; i < 10; i++) {
            dados += autores.get(i) + ", ";
        }
        dados += "\n" + "Local: " + local + "\n" + "Editora: " + editora + "\n" + "Duração: " + duracao + "\n" + "Ano: " + ano + "\n";
        return dados;
    }

    @Override
    public String toFile() {
        String dados = super.toFile() + "\r\n" + genero + "\r\n" + idioma + "\r\n";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ";";
        }
        dados += "\r\n" + local + "\r\n" + editora + "\r\n" + duracao + "\r\n" + ano + "\r\n";
        return dados;

    }
}
