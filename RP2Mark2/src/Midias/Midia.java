package Midias;

public class Midia {

    private static int codigoMaximo = 0;
    private int codigo;
    private String caminhoDoArquivo;
    private String titulo;
    private String descricao;

    public Midia(String caminhoDoArquivo, String titulo, String descricao) {
        this.caminhoDoArquivo = caminhoDoArquivo;
        this.titulo = titulo;
        this.descricao = descricao;
        this.codigo = 1 + codigoMaximo++;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCODIGO() {
        return codigo;
    }

    public String getCaminhoDoArquivo() {
        return caminhoDoArquivo;
    }

    public void setCaminhoDoArquivo(String caminhoDoArquivo) {
        this.caminhoDoArquivo = caminhoDoArquivo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return   "Código do Produto: " + codigo + "\n" +"Titulo: " + titulo + "\n" + "Caminho do Arquivo: " + caminhoDoArquivo + "\n" +  "Descrição: " + descricao;
    }

    /**
     * Metodo de salvamento de arquivos para TXT
     * @return String com arquivos a serem salvod
     */
    public String toFile() {
        return codigo + "\r\n" + caminhoDoArquivo + "\r\n" + titulo + "\r\n" + descricao;
    }


}
