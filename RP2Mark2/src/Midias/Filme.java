package Midias;

import java.util.List;

public class Filme extends Midia {

    private String genero;
    private String idioma;
    private String diretor;
    private List atoresPrincipais;
    private double duracao;
    private int ano;

    public Filme(String caminhoDoArquivo, String titulo, String descricao, String genero, String idioma, String diretor, List atoresPrincipais, double duracao, int ano) {
        super(caminhoDoArquivo, titulo, descricao);
        this.genero = genero;
        this.idioma = idioma;
        this.diretor = diretor;
        this.atoresPrincipais = atoresPrincipais;
        this.duracao = duracao;
        this.ano = ano;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    public List getAtoresPrincipais() {
        return atoresPrincipais;
    }

    public void setAtoresPrincipais(List atoresPrincipais) {
        this.atoresPrincipais = atoresPrincipais;
    }

    public double getDuracao() {
        return duracao;
    }

    public void setDuracao(double duracao) {
        this.duracao = duracao;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        String dados = super.toString() + "\n" + "Genêro: " + genero + "\n" + "Idioma: " + idioma + "\n" + "Diretor: " + diretor + "\n" + "Atores Principais: ";
        for (int i = 0; i < atoresPrincipais.size(); i++) {
            dados += atoresPrincipais.get(i) + ", ";
        }
        dados += "\n" + "Duração:" + duracao + "\n" + "Ano:" + ano + "\n";
        return dados;
    }
    @Override
    public String toFile(){
    String dados = super.toFile() + "\r\n" + genero + "\r\n" + idioma + "\r\n" + diretor + "\r\n";
        for (int i = 0; i < atoresPrincipais.size(); i++) {
            dados += atoresPrincipais.get(i) + ";";
        }
        dados +=  "\r\n" + duracao + "\r\n" + ano + "\r\n";
        return dados;
    
    
    
    }
            
}
