package Midias;

import java.util.List;

public class PartituraMusical extends Midia {

    private String genero;
    private List instrumentos;
    private List autores;
    private int ano;

    public PartituraMusical(String caminhoDoArquivo, String titulo, String descricao, String genero, List instrumento, List autores, int ano) {
        super(caminhoDoArquivo, titulo, descricao);
        this.genero = genero;
        this.instrumentos = instrumento;
        this.autores = autores;
        this.ano = ano;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public List getInstrumento() {
        return instrumentos;
    }

    public void setInstrumento(List instrumento) {
        this.instrumentos = (instrumento);
    }

    public List getAutores() {
        return autores;
    }

    public void setAutores(List autores) {
        this.autores = autores;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        String dados = super.toString() + "\n" + "Genero: " + genero + "\n" + "Instrumento: ";
        for (int i = 0; i < instrumentos.size(); i++) {
            dados += instrumentos.get(i) + ", ";
        }
        dados += "\n" + "Autores: ";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ", ";
        }
        dados += "\n " + "Ano: " + ano + "\n";
        return dados;

    }

    @Override
    public String toFile() {
        String dados = super.toFile() + "\r\n" + genero + "\r\n";
        for (int i = 0; i < instrumentos.size(); i++) {
            dados += instrumentos.get(i) + ";";
        }
        dados += "\r\n";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ";";
        }
        dados += "\r\n" + ano + "\r\n";
        return dados;

    }
}
