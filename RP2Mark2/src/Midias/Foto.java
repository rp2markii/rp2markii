package Midias;

import java.util.List;

/* @autor Matheus Marques */
public class Foto extends Midia {

    private String fotografo;
    private List pessoas;
    private String local;
    private String data;
    private double hora;

    public Foto(String caminhoDoArquivo, String titulo, String descricao, String fotografo, List pessoas, String local, String data, double hora) {
        super(caminhoDoArquivo, titulo, descricao);
        this.fotografo = fotografo;
        this.pessoas = pessoas;
        this.local = local;
        this.data = data;
        this.hora = hora;
    }

    public String getFotografo() {
        return fotografo;
    }

    public void setFotografo(String fotografo) {
        this.fotografo = fotografo;
    }

    public List getPessoas() {
        return pessoas;
    }

    public void setPessoas(List pessoas) {
        this.pessoas = pessoas;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getHora() {
        return hora;
    }

    public void setHora(double hora) {
        this.hora = hora;
    }

    @Override
    public String toString() {
        String dados = super.toString() + "\n" + "Fotografo: " + fotografo + "\n" + "Pessoas: ";
        for (int i = 0; i < pessoas.size(); i++) {
            dados += pessoas.get(i) + ", ";
        }
        dados += "\n" + "Local: " + local + "\n" + "Data: " + data + "\n" + "Hora: " + hora + "\n";
        return dados;
    }

    @Override
    public String toFile() {
        String dados = super.toFile() + "\r\n" + fotografo + "\r\n";
        for (int i = 0; i < pessoas.size(); i++) {
            dados += pessoas.get(i) + ";";
        }
        dados += "\r\n" + local + "\r\n" + data + "\r\n" + hora + "\r\n";
        return dados;
    }

}
