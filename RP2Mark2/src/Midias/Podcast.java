package Midias;

import java.util.List;

public class Podcast extends Midia {

    private String idioma;
    private List autores;
    private int ano;

    public Podcast(String caminhoDoArquivo, String titulo, String descricao, String idioma, List autores, int ano) {
        super(caminhoDoArquivo, titulo, descricao);
        this.idioma = idioma;
        this.autores = autores;
        this.ano = ano;

    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getIdioma() {
        return idioma;

    }

    public void setAutores(List autores) {
        this.autores = autores;
    }

    public List getAutores() {
        return autores;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getAno() {
        return ano;
    }

    @Override
    public String toString() {
        String dados = super.toString() + "\n" + "Idioma: " + idioma + "\n" + "Autores: ";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ",";
        }
        dados += "\n" + "Ano: " + ano + "\n";
        return dados;
    }

    @Override
    public String toFile() {
        String dados = super.toFile() + "\r\n" + idioma + "\r\n";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ";";
        }
        dados += "\r\n" + ano + "\r\n";
        return dados;
    }
}
