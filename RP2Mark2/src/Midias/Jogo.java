package Midias;

import java.util.List;

/* @autor Matheus Marques */
public class Jogo extends Midia {

    private String genero;
    private List autores;
    private int numeroDeJogadores;
    private boolean suporteARede;
    private int ano;

    public Jogo(String caminhoDoArquivo, String titulo, String descricao, String genero, List autores, int numeroDeJogadores, boolean suporteARede, int ano) {
        super(caminhoDoArquivo, titulo, descricao);
        this.genero = genero;
        this.autores = autores;
        this.numeroDeJogadores = numeroDeJogadores;
        this.suporteARede = suporteARede;
        this.ano = ano;
    }

    /**
     * Metodo para recuperar a String genero
     * @return Retorna a String genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * Metodo para modificar String genero
     * @param genero
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    public List getAutores() {
        return autores;
    }

    public void setAutores(List autores) {
        this.autores = autores;
    }

    public int getNumeroDeJogadores() {
        return numeroDeJogadores;
    }

    public void setNumeroDeJogadores(int numeroDeJogadores) {
        this.numeroDeJogadores = numeroDeJogadores;
    }

    public boolean isSuporteARede() {
        return suporteARede;
    }

    public void setSuporteARede(boolean suporteARede) {
        this.suporteARede = suporteARede;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        String dados = super.toString() + "\n" + "Genero: " + genero + "\n" + "Autores: ";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ", ";
        }
           dados += "\n" + "Numero de Jogadores: " + numeroDeJogadores + "\n" + "Suporte a Rede: " + suporteARede + "\n" + "Ano:" + ano + "\n";
           return dados;
    }

    @Override
    public String toFile() {
        String dados = super.toFile() + "\r\n" + genero + "\r\n" + numeroDeJogadores + "\r\n";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ";";
        }
        dados+= "\r\n" + suporteARede + "\r\n" + ano + "\r\n";
        return dados;
    
    }

}
