package Midias;

import java.util.List;

public class Musica extends Midia {

    private String genero;

    private String idioma;

    private List autores;

    private List interpretes;

    private double duracao;

    private int ano;

    public Musica(String caminhoDoArquivo, String titulo, String descricao, String genero, String idioma, List autores, List interpretes, double duracao, int ano) {
        super(caminhoDoArquivo, titulo, descricao);
        this.genero = genero;
        this.idioma = idioma;
        this.autores = autores;
        this.interpretes = interpretes;
        this.duracao = duracao;
        this.ano = ano;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public List getAutores() {
        return autores;
    }

    public void setAutores(List autores) {
        this.autores = autores;
    }

    public List getInterpretes() {
        return interpretes;
    }

    public void setInterpretes(List interpretes) {
        this.interpretes = interpretes;
    }

    public double getDuracao() {
        return duracao;
    }

    public void setDuracao(double duracao) {
        this.duracao = duracao;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        String dados = super.toString() + "\n" + "Genero: " + genero + "\n" + "Idioma: " + idioma + "\n" + "Autores: ";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ", ";
        }
        dados += "\n" + "Interpretes: ";
        for (int i = 0; i < interpretes.size(); i++) {
            dados += interpretes.get(i) + ", ";
            dados += "\n" + "Duração: " + duracao + "\n" + "Ano: " + ano + "\n";

        }
        return dados;

    }

    @Override
    public String toFile() {
        String dados = super.toFile() + "\r\n" + genero + "\r\n" + idioma + "\r\n";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ";";
        }
        dados += "\r\n";
        for (int i = 0; i < interpretes.size(); i++) {
            dados += interpretes.get(i) + ";";
        }
        dados += "\r\n" + duracao + "\r\n" + ano + "\r\n";

        return dados;
    }

}
