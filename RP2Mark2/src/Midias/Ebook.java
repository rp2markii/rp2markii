package Midias;

import java.util.List;

public class Ebook extends Midia {

    private String genero;
    private List autores;
    private String local;
    private String editora;
    private int numeroDePaginas;
    private int ano;

    public Ebook(String caminhoDoArquivo, String titulo, String descricao, String genero, List autores, String local, String editora, int numeroDePaginas, int ano) {
        super(caminhoDoArquivo, titulo, descricao);
        this.genero = genero;
        this.autores = autores;
        this.local = local;
        this.editora = editora;
        this.numeroDePaginas = numeroDePaginas;
        this.ano = ano;
    }

    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * @return the autores
     */
    public List getAutores() {
        return autores;
    }

    /**
     * @param autores the autores to set
     */
    public void setAutores(List autores) {
        this.autores = autores;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the editora
     */
    public String getEditora() {
        return editora;
    }

    /**
     * @param editora the editora to set
     */
    public void setEditora(String editora) {
        this.editora = editora;
    }

    /**
     * @return the numeroDePaginas
     */
    public int getNumeroDePaginas() {
        return numeroDePaginas;
    }

    /**
     * @param numeroDePaginas the numeroDePaginas to set
     */
    public void setNumeroDePaginas(int numeroDePaginas) {
        this.numeroDePaginas = numeroDePaginas;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        String dados = super.toString() + "\n" + "Genero: " + genero + "\n" + "Autores: ";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ", ";
        }
        dados += "\n" + "Local: " + local + "\n" + "Editora: " + editora + "\n" + "NumeroDePaginas: " + numeroDePaginas + "\n" + "Ano: " + ano + "\n";
        return dados;
    }

    @Override
    public String toFile() {
        String dados = super.toFile() + "\r\n" + genero + "\r\n";
        for (int i = 0; i < autores.size(); i++) {
            dados += autores.get(i) + ";";
        }
        dados += "\r\n" + local + "\r\n" + editora + "\r\n" + numeroDePaginas + "\r\n" + ano + "\r\n";
        return dados;
    }
}
